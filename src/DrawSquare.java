import javax.swing.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DrawSquare extends JPanel {


    private int width;
    private int height;
    private int ox = 10;
    private int oy = 10;
    private int offsetX;
    private int offsetY;
    private int currentX, currentY, oldX, oldY;
    // Image in which we're going to draw
    private Image image;
    // Graphics2D object ==> used to draw on
    private Graphics2D g2;

    public DrawSquare() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                // save coord x,y when mouse is pressed
                oldX = e.getX();
                oldY = e.getY();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {

                // coord x,y when drag mouse
                currentX = e.getX() ;
                currentY = e.getY();

                offsetX = currentX - oldX;
                offsetY = currentY - oldY;

                g2.fillRect(oldX, oldY, offsetX , offsetY);
                repaint();



            }
        });

    }
/**
 private class ColourListener implements ActionListener {
 public void actionPerformed(ActionEvent e) {
 Component source = (Component) e.getSource();
 if (source == redButton) {
 redPanel.setBackground(Color.RED);
 } else if (source == blueButton) {
 bluePanel.setBackground(Color.BLUE);
 } else if (source == whiteButton) {
 whitePanel.setBackground(Color.WHITE);
 }
 }
 }
 **/


    /**
     public DrawRectangle1(){
     setLayout(null);
     width = 0;
     height = 0;
     JButton button = new JButton("Rectangle");
     button.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
    width = 200;
    height = 150;
    repaint();
    }
    });
     button.setBounds(400, 100, 120, 30);
     add(button);
     }

     public DrawRectangle2(){
     setLayout(null);
     width = 0;
     height = 0;
     JButton button = new JButton("Square");
     button.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
    width = 200;
    height = 200;
    repaint();
    }
    });
     button.setBounds(400, 80, 120, 30);
     add(button);
     }
     **/

    protected void paintComponent(Graphics g) {
        if (image == null) {
            // image to draw null ==> we create
            image = createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) image.getGraphics();
            // enable antialiasing
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            // clear draw area
            clear();
        }

        g.drawImage(image, 0, 0, null);
    }

    // now we create exposed methods
    public void clear() {
        g2.setPaint(Color.white);
        // draw white on entire draw area to clear
        g2.fillRect(0, 0, getSize().width, getSize().height);
        g2.setPaint(Color.red);
        repaint();
    }
}