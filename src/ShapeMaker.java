import javax.swing.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ShapeMaker extends JPanel {



    // initialise properties for mouse movement
    private int offsetX;
    private int offsetY;
    private int currentX, currentY, oldX, oldY;

    // initialise image and graphic
    private Image image;
    private Graphics2D g2;

    // constructor for ShapeMaker
    public ShapeMaker() {

        initShapeButtons();
        initColourButtons();
    }

    public void initShapeButtons(){
        //create a layoutmanager for the panel that
        // maker will be added to, and create drawing area
        this.setLayout(new GridLayout(3,5));
        this.add(Box.createRigidArea(new Dimension(600,100)));

        // create group of radiobuttons to house
        // actionlisteners that change the shape drawn
        ButtonGroup sbgroup = new ButtonGroup();
        JRadioButton elipseRadio = new JRadioButton("Elipse");
        JRadioButton rectRadio = new JRadioButton("Rect");
        JRadioButton squareRadio = new JRadioButton("Square");
        // add radiobuttons to frame
        this.add(elipseRadio);
        this.add(rectRadio);
        this.add(squareRadio);
        sbgroup.add(elipseRadio);
        sbgroup.add(rectRadio);
        sbgroup.add(squareRadio);

        //create a box to house the buttons
        Box sbot = Box.createHorizontalBox();
        sbot.add(elipseRadio);
        sbot.add(rectRadio);
        sbot.add(squareRadio);
        this.add(sbot);

        //create actionlister and attach to buttons
        // select shape to draw from button
        ActionListener actionListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == elipseRadio) {
                    drawOval();
                }
                else if (e.getSource() == squareRadio) {
                    drawSqr();
                }
                else if (e.getSource() == rectRadio) {
                    drawRectangle();
                }
            }
        };

        elipseRadio.addActionListener(actionListener);
        squareRadio.addActionListener(actionListener);
        rectRadio.addActionListener(actionListener);
    }

    public void initColourButtons(){
        //same boilerplate as shape buttons
        ButtonGroup bgroup = new ButtonGroup();
        JRadioButton blueRadio = new JRadioButton();
        JRadioButton yellowRadio = new JRadioButton();
        JRadioButton redRadio = new JRadioButton();
        JRadioButton greenRadio = new JRadioButton();
        JRadioButton blackRadio = new JRadioButton();

        this.add(blueRadio);
        this.add(yellowRadio);
        this.add(redRadio);
        this.add(greenRadio);
        this.add(blackRadio);

        bgroup.add(blueRadio);
        bgroup.add(yellowRadio);
        bgroup.add(redRadio);
        bgroup.add(greenRadio);
        bgroup.add(blackRadio);

        Box bot = Box.createHorizontalBox();
        bot.add(blueRadio);
        bot.add(yellowRadio);
        bot.add(redRadio);
        bot.add(greenRadio);
        bot.add(blackRadio);

        this.add(bot, BorderLayout.SOUTH);
        blueRadio.setText("Blue");
        yellowRadio.setText("Yellow");
        redRadio.setText("Red");
        greenRadio.setText("Green");
        blackRadio.setText("Black");

        ActionListener radioListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == blueRadio) {
                    g2.setPaint(Color.BLUE);
                }
                else if (e.getSource() == yellowRadio) {
                    g2.setPaint(Color.YELLOW);
                }
                else if (e.getSource() == redRadio) {
                    g2.setPaint(Color.RED);
                }
                else if (e.getSource() == greenRadio) {
                    g2.setPaint(Color.GREEN);
                }
                else if (e.getSource() == blackRadio) {
                    g2.setPaint(Color.BLACK);
                }
            }
        };

        blueRadio.addActionListener(radioListener);
        yellowRadio.addActionListener(radioListener);
        redRadio.addActionListener(radioListener);
        greenRadio.addActionListener(radioListener);
        blackRadio.addActionListener(radioListener);

    }


    // Set of functions with shapes and variables
    public void rect(){

        g2.fillRect(oldX, oldY, offsetX , offsetY);

    }

    public void sqr(){

        g2.fillRect(oldX, oldY, offsetX , offsetY);
    }

    public void circle(){


        g2.fillOval(oldX, oldY, offsetX , offsetY/2);

    }

    // mouseListener on click to draw shapes while dragged
    public void drawRectangle() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                // save coord x,y when mouse is pressed
                oldX = e.getX();
                oldY = e.getY();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                // coord x,y when drag mouse
                currentX = e.getX() ;
                currentY = e.getY();
                offsetX = currentX - oldX;
                offsetY = currentY - oldY;
                rect();
                repaint();
            }
        });
    }

    //boilerplate as above
    public void drawSqr() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                // save coord x,y when mouse is pressed
                oldX = e.getX();
                oldY = e.getY();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {

                // coord x,y when drag mouse

                currentX = e.getX() ;
                currentY = e.getY();

                offsetX = currentX - oldX;
                offsetY = currentY - oldY;

                sqr();
                repaint();
            }

        });
    }

    //boilerplate as above
    public void drawOval() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                // save coord x,y when mouse is pressed
                oldX = e.getX();
                oldY = e.getY();
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                // coord x,y when drag mouse
                currentX = e.getX() ;
                currentY = e.getY();
                offsetX = currentX - oldX;
                offsetY = currentY - oldY;
                circle();
                repaint();
            }
        });
    }







    protected void paintComponent(Graphics g) {
    if (image == null) {
        // image to draw null ==> we create
        image = createImage(getSize().width, getSize().height);
        g2 = (Graphics2D) image.getGraphics();


        // clear draw area
        clear();
    }

    g.drawImage(image, 0, 0, null);
}

    // now we create exposed methods
    public void clear() {
        g2.setPaint(Color.white);
        // draw white on entire draw area to clear
        g2.fillRect(0, 0, getSize().width, getSize().height);

        repaint();
    }
}