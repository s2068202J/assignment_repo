import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class maker extends JFrame {

    /**
     *
     */
    private JButton clearBtn, blackBtn, blueBtn, rectButton, squareButton;



    private JTextField filename;
    private maker gui;
    private JTextArea fileContents;
    private JPanel filePanel;
    private maker maker;

    private static String string;


    // construtor for maker class; initialises components
    public maker() {
        super("MillionDollarXtreme");
        gui = this;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create JPanel p1 to hold components
        JPanel p1 = new JPanel();
        p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));

        //add draw component to the panel
        p1.add(new ShapeMaker());

        // create panel for file open component
        JPanel filePanel = new JPanel();

        // Create text-field component to attach listener
        // for filepath. Add the label and component to panel
        JLabel file = new JLabel("Enter Filename: ");
        filename = new JTextField(30);
        filename.addKeyListener(new FileNameListener());
        filePanel.add(file);
        filePanel.add(filename);

        // create text-area component to display file body
        fileContents = new JTextArea(string);

        //create a panel to house scrollbar
        // use LayoutManager to set the search field CENTER
        // and results at SOUTH
        // Boilerplate from qut resources
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        JScrollPane scroller = new JScrollPane(fileContents);
        scroller
                .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        mainPanel.add(filePanel, BorderLayout.CENTER);
        mainPanel.add(scroller, BorderLayout.SOUTH);

        //select frame and apply a new layout manager
        // set Y_AXIS so it sits underneath other component
        // add panels to the frame and separate them
        // with a verticalstruct and glue
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        getContentPane().add(p1);
        getContentPane().add(Box.createVerticalStrut(20));
        getContentPane().add(mainPanel);
        getContentPane().add(Box.createVerticalStrut(20));
        setPreferredSize(new Dimension(800, 400));

        //pack components and set to visible
        pack();
        setVisible(true);

    }




    void addContents(String text) {
        fileContents.setText(text);
    }

    //create listener event for parsing text to
    //text string
    // boilerplate
    class FileNameListener extends KeyAdapter {
        public void keyReleased(KeyEvent key) {
            if (key.getKeyCode() == KeyEvent.VK_ENTER) {
                SwingWorker<String, Void> data = new FileContentReader(filename
                        .getText(), gui);
                data.execute();
                filename.setText("");
            }
        }
    }
    public void run() {
        createAndShowGUI();
    }

    private void createAndShowGUI() {

        JFrame.setDefaultLookAndFeelDecorated(true);

        // Display the window.


        gui.setLocation(new Point(200, 200));

        gui.setVisible(true);
    }



    // main function for running program
    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        new maker();
    }

}
