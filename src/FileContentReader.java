import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileContentReader extends SwingWorker<String, Void> {

   public static String NEW_LINE = System.getProperty("line.separator");

   private StringBuffer contents;

   private File file;

   private maker handle;

   public FileContentReader(String filename, maker gui) {
      file = new File(filename);
      contents = new StringBuffer();
      handle = gui;
   }

   @Override
   protected String doInBackground() throws Exception {
      if (file.exists()) {
         BufferedReader reader = new BufferedReader(new FileReader(file));
         String line;
         while ((line = reader.readLine()) != null) {
            contents.append(line).append(NEW_LINE);
         }
         reader.close();
      }
      return new String(contents);
   }

   @Override
   protected void done() {
      handle.addContents(new String(contents));
   }
}
